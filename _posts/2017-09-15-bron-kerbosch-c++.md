---
layout: post
title:  "Bron-Kerbosch C++ implementation"
date:   2017-09-15
desc: "Bron-Kerbosch C++ implementation."
keywords: "Bron-Kerbosch,C++"
categories: [C++]
tags: [C++]
icon: fa-rss
---

Yet another [implementation](https://gitlab.com/Gluttton/BronKerbosch) of the [Brohn-Kerbosch algorithm](https://en.wikipedia.org/wiki/Bron%E2%80%93Kerbosch_algorithm).
It is non-optimized version but very flexible and allows to solve a varity of problems:  
    - finding a single maximal clique;  
    - finding all maximal cliques;  
    - finding a maximum clique;  
    - finding a maximum weight clique;  
    - finding a single maximal independent set;  
    - finding all maximal independent set;  
    - etc...  

